#ifndef NODE_H
#define NODE_H

#include <trianglesystem/gmtrianglesystem.h>

#include <array>

class Node
{
public:
    Node();
    Node(GMlib::TSVertex<float>* v);

    GMlib::Array<GMlib::TSTriangle<float> *> getTriangles();
    GMlib::TSEdge<float> *getNeighbor(Node &n);
    void setZ(float _z);
    GMlib::TSVertex<float>* getVertex();

    private:
    GMlib::TSVertex<float>* _v;

};

#endif // NODE_H
