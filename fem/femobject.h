#ifndef FEMOBJECT_H
#define FEMOBJECT_H

#include <core/containers/gmarraylx.h>
#include <trianglesystem/gmtrianglesystem.h>
#include "../fem/node.h"



class FEMObject : public GMlib::TriangleFacets<float>
{
public:
    FEMObject();
    FEMObject(float radius, int points, int rows, float f);

    void regularTriangulation(float r, int n, int m);
    void randomTriangulation();
    void Computation();
    void stiffnessMatrix(int _i);
    void nonDiagonalElements(int _i);
    void diagonalElements(int _i);
    GMlib::Vector<GMlib::Vector<float, 2>, 3> findVectors(GMlib::TSEdge<float>* edge);
    GMlib::Vector<GMlib::Vector<float, 2>, 3> findVectorsDiagonal(GMlib::TSTriangle<float>* triangle, GMlib::TSVertex<float>* vertex);
    void printMatrix();
    void findLoadVector(float f);
    void solveMatrixEq();
private:
    GMlib::ArrayLX<Node> _nodes;
    int numberOfBoundaryPoints;
    GMlib::DMatrix<float> _A;
    GMlib::DVector<float> _b;
    float _f;

};
#endif // FEMOBJECT_H
