#include "node.h"

Node::Node()
{

}

Node::Node(GMlib::TSVertex<float> *v)
{
    _v = v;
}

GMlib::Array<GMlib::TSTriangle<float> *> Node::getTriangles()
{
    return  _v->getTriangles();
}

GMlib::TSEdge<float> *Node::getNeighbor(Node &n)
{
    auto edges = _v->getEdges();
    for(auto i = 0; i < edges.size(); i++) {
        if(edges[i]->getOtherVertex(*this->getVertex()) == n.getVertex()) {
            return  edges[i];
        }
    }
    return nullptr;

}

void Node::setZ(float _z)
{
    _v->setZ(_z);
}

GMlib::TSVertex<float>* Node::getVertex()
{
    return _v;
}
