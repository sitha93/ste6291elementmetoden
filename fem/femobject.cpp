#include "femobject.h"

#include <iostream>
#include <chrono>
#include <random>

FEMObject::FEMObject()
{
    _f = 1;
    regularTriangulation(5, 7, 4);
    randomTriangulation();
}

FEMObject::FEMObject(float radius, int points, int rows, float f)
{
    _f = f;
    regularTriangulation(radius, points, rows);
    randomTriangulation();
}

void FEMObject::regularTriangulation(float r, int n, int m)
{
    this->insertAlways(GMlib::TSVertex<float>(GMlib::Point<float, 2>(0.0, 0.0)));

    for(auto i = 0; i < m ; i++) {
        auto l = GMlib::Point<float, 2>((i+1)*r/m, 0);
        auto counter = (i+1) *n;
        numberOfBoundaryPoints = counter;
        for(auto j = 0; j < counter; j++) {
            auto alpha = GMlib::Angle(2*M_PI*j/counter);
            auto rot = GMlib::SqMatrix<float,2>(alpha);
            auto vertex = rot*l;
            this->insertAlways(GMlib::TSVertex<float>(GMlib::Point<float, 2>(vertex[0], vertex[1])));
        }
    }

    this->reverse();
}

void FEMObject::randomTriangulation()
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    for(auto i = 0; i < 10 ; i++) {
        shuffle (this->getVertex(numberOfBoundaryPoints), this->getVertex(this->size()-1), std::default_random_engine(seed));
    }

    for(auto i = 0; i < ((this->size()/2) - (numberOfBoundaryPoints/2)); i++ ) {
        this->removeBack();
    }
}

void FEMObject::Computation()
{
    for(auto i = numberOfBoundaryPoints; i< this->size(); i++) {
        auto node = Node(this->getVertex(i));
        _nodes += node;
    }

    _A = GMlib::DMatrix<float>(_nodes.size(), _nodes.size());

    for(int i = 0; i < _A.getDim1() ; i++) {
        for(int j=0; j < _A.getDim2() ; j++) {
            _A[i][j] = 0 ;
        }
    }

    stiffnessMatrix(_nodes.size());
    findLoadVector(_f);
//    printMatrix();
    solveMatrixEq();
//    printMatrix();
}

void FEMObject::stiffnessMatrix(int _i)
{
    nonDiagonalElements(_i);
    diagonalElements(_i);
}

void FEMObject::nonDiagonalElements(int _i)
{
    for(int i = 0; i < _i ; i++) {
        for(int j=0; j < i ; j++) {
            GMlib::TSEdge<float>* edge = _nodes[i].getNeighbor(_nodes[j]);
            if(edge != nullptr) {
                auto vectors = findVectors(edge);
                auto d = vectors[0];
                auto a1 = vectors[1];
                auto a2 = vectors[2];

                auto dd = 1.0f / (d.getLength() * d.getLength());
                auto dh1 = dd * (a1 * d);
                auto dh2 = dd * (a2 * d);
                auto area1 = std::abs(d ^ a1);
                auto area2 = std::abs(d ^ a2);
                auto h1 = dd * (area1 * area1);
                auto h2 = dd * (area2 * area2);

                _A[i][j] = ((dh1 * (1 - dh1)/h1) - dd) * area1/2
                         + ((dh2 * (1 - dh2)/h2) - dd) * area2/2;
                _A[j][i] = _A[i][j];
            }
        }
    }
}

void FEMObject::diagonalElements(int _i)
{
    for(int i = 0; i < _i; i++) {
        float sum = 0.0f;
        auto triangles = _nodes[i].getTriangles();
        for(int j = 0; j < triangles.size(); j++) {
            auto vectors = findVectorsDiagonal(triangles[j], _nodes[i].getVertex());
            auto vec1 = vectors[0];
            auto vec2 = vectors[1];
            auto vec3 = vectors[2];

            auto Tk = (vectors[2] * vectors[2]) / (2 * (std::abs(vectors[0] ^ vectors[1])));
            sum += Tk;

        }
        _A[i][i] = sum;
    }
}

GMlib::Vector<GMlib::Vector<float, 2>, 3> FEMObject::findVectors(GMlib::TSEdge<float> *edge)
{
    GMlib::Vector<GMlib::Vector<float, 2>, 3> vectors;

    auto triangleOne = edge->getTriangle()[0];
    auto triangleTwo = edge->getTriangle()[1];

    auto vertexOne = edge->getFirstVertex();
    auto vertexTwo = edge->getLastVertex();

    vectors[0] = vertexTwo->getPosition() - vertexOne->getPosition();

    for(int i = 0; i < 3; i++) {
        if(triangleOne->getVertices()[i] != vertexOne && triangleOne->getVertices()[i] != vertexTwo) {
            auto vec =  triangleOne->getVertices()[i]->getPosition() - vertexOne->getPosition();
            vectors[1] = vec;
        }
        if(triangleTwo->getVertices()[i] != vertexOne && triangleTwo->getVertices()[i] != vertexTwo) {
            auto vec =  triangleTwo->getVertices()[i]->getPosition() - vertexOne->getPosition();
            vectors[2] = vec;
        }
    }

    return vectors;
}


GMlib::Vector<GMlib::Vector<float, 2>, 3> FEMObject::findVectorsDiagonal(GMlib::TSTriangle<float> *triangle, GMlib::TSVertex<float>* vertex)
{
    GMlib::Vector<GMlib::Vector<float, 2>, 3> vectors;
    auto vertices = triangle->getVertices();

    for(int i = 0; i < 3 ; i++) {
        if(vertices[i] == vertex && i != 0) {
            auto vert = vertices[0];
            vertices[0] = vertices[i];
            vertices[i] = vert;
        }
    }

    vectors[0] = vertices[1]->getPosition() - vertices[0]->getPosition();
    vectors[1] = vertices[2]->getPosition() - vertices[0]->getPosition();
    vectors[2] = vertices[2]->getPosition() - vertices[1]->getPosition();
    return vectors;
}

void FEMObject::printMatrix()
{
    for(int i = 0 ; i < _A.getDim1(); i++) {
        for(int j = 0; j < _A.getDim2(); j++) {
            std::cout << _A[i][j] << " ";
        }
        std::cout << "\n" << std::endl;
    }
}

void FEMObject::findLoadVector(float f)
{
    _b = GMlib::DVector<float>(_nodes.size());
    for(int i = 0; i < _nodes.size(); i++){
        auto triangles = _nodes[i].getTriangles();
        auto sum = 0.0f;
        for(int j = 0; j < triangles.size(); j++) {
            auto area = triangles[j]->getArea2D();
            sum += area;
        }

        auto val = f/3 * sum;
        _b[i] = val;

    }
}

void FEMObject::solveMatrixEq()
{
    _A.invert();
    auto x = _A*_b;

    for(int i = 0; i < _nodes.size(); i++) {
        _nodes[i].setZ(x[i]);
    }
}
